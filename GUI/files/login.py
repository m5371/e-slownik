# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'Login.ui'
#
# Created by: PyQt5 UI code generator 5.15.2
#
# WARNING: Any manual changes made to this file will be lost when pyuic5 is
# run again.  Do not edit this file unless you know what you are doing.


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(304, 378)
        MainWindow.setFixedSize(304, 378)
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(":/loga/images/fav.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        MainWindow.setWindowIcon(icon)
        MainWindow.setStyleSheet("background-color: rgb(55 ,55, 55);\n"
                                 "color: rgb(255, 255, 255);")
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.label = QtWidgets.QLabel(self.centralwidget)
        self.label.setGeometry(QtCore.QRect(70, 30, 171, 91))
        self.label.setStyleSheet(f"border-image: url(:/loga/images/Logo1.png);")
        self.label.setText("")
        self.label.setObjectName("label")
        self.lineEdit = QtWidgets.QLineEdit(self.centralwidget)
        self.lineEdit.setGeometry(QtCore.QRect(70, 240, 171, 21))
        self.lineEdit.setStyleSheet("background-color: rgb(216, 216, 216);\n"
                                    "color: rgb(0, 0, 0);")
        self.lineEdit.setEchoMode(QtWidgets.QLineEdit.Password)
        self.lineEdit.setAlignment(QtCore.Qt.AlignLeading | QtCore.Qt.AlignLeft | QtCore.Qt.AlignVCenter)
        self.lineEdit.setObjectName("lineEdit")
        self.lineEdit_2 = QtWidgets.QLineEdit(self.centralwidget)
        self.lineEdit_2.setGeometry(QtCore.QRect(70, 200, 171, 21))
        self.lineEdit_2.setStyleSheet("background-color: rgb(216, 216, 216);\n"
                                      "color: rgb(0, 0, 0);")
        self.lineEdit_2.setInputMask("")
        self.lineEdit_2.setObjectName("lineEdit_2")
        self.label_2 = QtWidgets.QLabel(self.centralwidget)
        self.label_2.setGeometry(QtCore.QRect(70, 140, 171, 21))
        font = QtGui.QFont()
        font.setPointSize(13)
        font.setBold(True)
        font.setWeight(75)
        self.label_2.setFont(font)
        self.label_2.setAlignment(QtCore.Qt.AlignCenter)
        self.label_2.setObjectName("label_2")
        self.pushButton = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton.setGeometry(QtCore.QRect(100, 290, 111, 23))
        self.pushButton.setStyleSheet("background-color: rgb(216, 216, 216);\n"
                                      "color: rgb(0, 0, 0);")
        self.pushButton.setObjectName("pushButton")
        self.pushButton_2 = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_2.setGeometry(QtCore.QRect(120, 330, 75, 23))
        self.pushButton_2.setStyleSheet("background-color: rgb(216, 216, 216);\n"
                                        "color: rgb(0, 0, 0);")
        self.pushButton_2.setObjectName("pushButton_2")
        MainWindow.setCentralWidget(self.centralwidget)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

        self.pushButton.clicked.connect(lambda: self.login(self.lineEdit_2.text(),
                                                           self.lineEdit.text()))

        self.pushButton_2.clicked.connect(lambda: self.reg_window())

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "Login"))
        self.lineEdit.setPlaceholderText(_translate("MainWindow", "Hasło"))
        self.lineEdit_2.setPlaceholderText(_translate("MainWindow", "Login"))
        self.label_2.setText(_translate("MainWindow", "Logowanie"))
        self.pushButton.setText(_translate("MainWindow", "Zaloguj"))
        self.pushButton_2.setText(_translate("MainWindow", "Rejestracja"))

    def log(self):
        self.label3 = QtWidgets.QLabel(self.centralwidget)
        self.label3.setGeometry(QtCore.QRect(0, 0, 311, 381))
        self.label3.setStyleSheet("background-color: rgb(55, 55, 55, 100);")
        self.label3.setText("")
        self.label3.setObjectName("label3")
        self.label3.show()
        self.label4 = QtWidgets.QLabel(self.centralwidget)
        self.label4.setGeometry(QtCore.QRect(70, 140, 171, 31))
        self.label4.setStyleSheet("background-color: rgb(55, 55, 55, 100);")
        font = QtGui.QFont()
        font.setPointSize(14)
        font.setBold(True)
        self.label4.setFont(font)
        self.label4.setAlignment(QtCore.Qt.AlignVCenter)
        self.label4.setText("Loguje ...")
        self.label4.setObjectName("label4")
        self.label4.show()

    def log_exe(self):
        self.label3.hide()
        self.label4.hide()